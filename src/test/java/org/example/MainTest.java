package org.example;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MainTest {

    WebDriver driver;
    int countRecipesEqual;
    int pageCount;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:/Users/79372/Desktop/chromedriver.exe");
        driver = new ChromeDriver();

    }

    @Test
    public void countRecipesEqualTest() {
        // Logging in with the specified credentials
        login("voropali@fel.cz", "21042003Fkbyf!");

        // Navigate to the index2.php page
        driver.findElement(By.cssSelector("[href='index2.php']")).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.urlToBe("http://wa.toad.cz/~voropali/01/index2.php"));

        // Count recipes with the certain context
        countRecipesEqual = 0;
        String desiredContent = "FOR_TS1";

        // Count how many pages
        pageCount = 1;
        List<WebElement> pages = driver.findElements(By.className("page"));
        for (WebElement page : pages) {
            if (page != null) {
                pageCount++;
            }
        }
        int i = 1;
        while (i != (pageCount + 1)) {
            // Get all recipes on the page
            List<WebElement> recipes = driver.findElements(By.className("span8"));
            for (WebElement recipe : recipes) {
                String content = recipe.getText();
                if (content.contains(desiredContent)) {
                    countRecipesEqual++;
                }
            }
            if (i != pageCount) {
                i++;
                WebElement nextPageLink = driver.findElement(By.cssSelector("[href='index2.php?page=" + i + "']"));
                nextPageLink.click();
                WebDriverWait wait2 = new WebDriverWait(driver, Duration.ofSeconds(5));
                wait2.until(ExpectedConditions.stalenessOf(nextPageLink));
                wait2.until(ExpectedConditions.urlContains("page=" + i));
            } else {
                break;
            }
        }
        int expected = 2;
        Assertions.assertEquals(expected, countRecipesEqual, "The number of identical recipes is incorrect");

        // Close the browser
        driver.quit();
    }

    @Test
    public void deleteRecipeTest() {
        // Logging in with the specified credentials
        login("voropali@fel.cz", "21042003Fkbyf!");
        driver.findElement(By.cssSelector("[href='edit.php']")).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.urlToBe("http://wa.toad.cz/~voropali/01/edit.php"));

        // Retrieve the current value and the value to be deleted
        String deletedValue = "FOR_TS1";
        String current = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[2]//td[3]")).getText();

        // Check if the current value matches the value to be deleted
        if (current.equals(deletedValue)) {
            // If it matches, find the delete link and perform the deletion
            WebElement deleteLink = driver.findElement(By.name("delete-link"));
            JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
            jsExecutor.executeScript("arguments[0].scrollIntoView(true);", deleteLink);
            deleteLink.click();
        }

        // Verify if the recipe was successfully deleted
        String control = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[2]//td[3]")).getText();
        boolean noteElement;
        if (control != deletedValue && current.equals(deletedValue)) {
            noteElement = true;
        } else {
            noteElement = false;
        }
        assertEquals(noteElement, true, "Recipe was not deleted");

        // Close the browser
        driver.quit();
    }


    public void login(String email, String password) {
        // Navigate to the website
        driver.get("http://wa.toad.cz/~voropali/01/login.php");

        // Find and enter the email
        driver.findElement(By.name("email")).sendKeys(email);

        // Find and enter the password
        driver.findElement(By.name("password")).sendKeys(password);

        //Click on the login button
        driver.findElement(By.name("btn_submit_auth")).click();

        //Wait for the page to load
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.urlToBe("http://wa.toad.cz/~voropali/01/myrecipes.php"));
    }


    @Test
    public void ErrorUserEqualEmailTest() {
        // Logging in with the specified credentials
        login("voropali@fel.cz", "21042003Fkbyf!");

        // Find input elements for title, content, and email
        WebElement inputTitle = driver.findElement(By.className("title"));
        WebElement inputContent = driver.findElement(By.className("content"));
        WebElement inputEmail = driver.findElement(By.className("email"));

        // Set values for title, content, and email fields
        String titleText = "FFFFFFF";
        String contentText = "ffffffff";
        String emailText = "00@yandex.ru";
        inputTitle.sendKeys(titleText);
        inputContent.sendKeys(contentText);
        inputEmail.sendKeys(emailText);

        // Find and click the submit link
        WebElement submitLink = driver.findElement(By.className("submit"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].scrollIntoView(true);", submitLink);
        submitLink.click();

        // Wait for the URL to be "http://wa.toad.cz/~voropali/01/myrecipes.php"
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        wait.until(ExpectedConditions.urlToBe("http://wa.toad.cz/~voropali/01/myrecipes.php"));

        // Find the error element and retrieve the error text
        WebElement error = driver.findElement(By.className("error"));
        String errorText = error.getText();
        String expectedErrorText = "Enter your email that you used when logging in";
        assertEquals(expectedErrorText, errorText, "The alert text does not correspond");

        // Close the browser
        driver.quit();
    }

    @Test
    public void ErrorRegistrationTest() {
        // Open the registration page
        driver.get("http://wa.toad.cz/~voropali/01/registration.php");

        // Enter the name, number, email, and password
        driver.findElement(By.name("name")).sendKeys("Alina");
        driver.findElement(By.name("number")).sendKeys("20");
        driver.findElement(By.name("email")).sendKeys("voropali@fel.cz");
        driver.findElement(By.name("password")).sendKeys("21Fg2121!");

        // Click on the submit button
        driver.findElement(By.className("submit")).click();

        // Retrieve the error text and compare it with the expected error text
        String errorText = driver.findElement(By.className("error")).getText();
        String expectedErrorText = "Email used";
        assertEquals(expectedErrorText, errorText, "The error text does not correspond");

        // Close the browser
        driver.quit();
    }


    @Test
    public void logoutTest() {
        login("voropali@fel.cz", "21042003Fkbyf!");

        // Click on the logout button
        WebElement logOutLink = driver.findElement(By.className("logout"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].scrollIntoView(true);", logOutLink);
        logOutLink.click();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
        wait.until(ExpectedConditions.urlToBe("http://wa.toad.cz/~voropali/01/index.php"));

        // Verify that the login was successful
        String expectedUrl = "http://wa.toad.cz/~voropali/01/index.php";
        String actualUrl = driver.getCurrentUrl();

        assertEquals(expectedUrl, actualUrl, "Logout test failed!");

        // Close the browser
        driver.quit();
    }

    @ParameterizedTest
    @CsvSource({"sil@gmail.com, 1234Fkbyf!, true", "vac@gmail.com, 1111Fkbyf!, true", "max@gmail.com, 2222Fkbyf, false"})
    public void param_loginCheck_Test(String email, String password, boolean isCorrect) {
        // Navigate to the website
        driver.get("http://wa.toad.cz/~voropali/01/login.php");

        // Find and enter the email
        driver.findElement(By.name("email")).sendKeys(email);

        // Find and enter the password
        driver.findElement(By.name("password")).sendKeys(password);

        // Click on the login button
        driver.findElement(By.name("btn_submit_auth")).click();

        // Wait for the page to load
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        if (isCorrect) {
            wait.until(ExpectedConditions.urlToBe("http://wa.toad.cz/~voropali/01/myrecipes.php"));
        } else {
            wait.until(ExpectedConditions.urlToBe("http://wa.toad.cz/~voropali/01/login.php"));
        }

        // Verify that the login was successful
        boolean isCorrespond = ("http://wa.toad.cz/~voropali/01/myrecipes.php").equals(driver.getCurrentUrl());

        assertEquals(isCorrespond, isCorrect);

        // Close the browser
        driver.quit();
    }
}
